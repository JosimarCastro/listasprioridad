
package coladeprioridad;

public class nodo {
    
    nodo siguiente;
    int dato;
    int prioridad;

    public nodo(nodo siguiente, int dato, int prioridad) {
        this.siguiente = null;
        this.dato = dato;
        this.prioridad=0;
    }
    
    public nodo(){
        siguiente = null;
    }
    
}
