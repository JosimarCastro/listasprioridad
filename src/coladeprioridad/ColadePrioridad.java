
package coladeprioridad;

import java.util.Scanner;

public class ColadePrioridad {

    public static void main(String[] args) {
        
        Cola col = new Cola();
        
        Scanner entrada = new Scanner(System.in);
        int i;
        
        System.out.println("Ingrede la cantidad de nodos a introducir: ");      
        int n = entrada.nextInt();
        
        for(i=0;i<n;i++){
            System.out.println("Ingrese un dato al Nodo");
            int x = entrada.nextInt();
            System.out.println("Ingrese una prioridad al Nodo");
            int y = entrada.nextInt();

            col.insertar(x, y);   
        }
        
        System.out.println("---------------------------");

        col.mostrar();
        col.cambiarprioridad();
   
        System.out.println("---------------------------");
        System.out.println("Nodos: ");
        col.mostrar();
        System.out.println("---------------------------");
        System.out.println("Nodos con Prioridad Nueva: ");
        col.mostrar();

        
    }
     

     
        
      
    
    
}
